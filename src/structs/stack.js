import { append, is, base } from './internals/list'
import { copy } from '../functions/copy'

export const stack = (...values) =>
  Object.create(
    Object.assign({
      add: value => append(stack, value, values),
      remove: () => stack(...values.slice(0, values.length - 1)),
      toString: () => `stack [${values.join(' ')}]`,
      copy: () => stack(...copy(values)),
      is: is(stack),
      constructor: stack
    }, base(...values)))
