export const append = (wrapper, value, array) => wrapper(...array.concat([value]))

export const is = constructor => factory => constructor === factory

export const base = (...values) => ({
  get: index => values[index],
  toArray: () => values.slice(0),
  get length() { return values.length },
  get __perpetuum__() { return true }
})
