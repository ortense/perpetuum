import { append, is, base } from './internals/list'
import { copy } from '../functions/copy'

export const queue = (...values) =>
  Object.create(
    Object.assign({
      add: value => append(queue, value, values),
      remove: () => queue(...values.slice(1)),
      toString: () => `queue [${values.join(' ')}]`,
      copy: () => queue(...copy(values)),
      is: is(queue),
      constructor: queue
    }, base(...values)))
