export const curry = (fn, ...args) =>
  args.length < fn.length
    ? (...more) => curry(fn, ...args, ...more)
    : fn(...args)
