export const or = (...fns) =>
  (...args) => fns.some(fn => fn(...args))
