export const memoize = fn => {
  const memo = {}

  return (...args) => {
    if (args in memo) return memo[args]
    return (memo[args] = fn(...args))
  }
}
