import { curry } from './curry'

export const reduce = curry((fn, initial, struct) => struct.__perpetuum__
  ? struct.toArray().reduce(fn, initial)
  : struct.reduce(fn, initial))
