export const and = (...fns) =>
  (...args) => fns.every(fn => fn(...args))
