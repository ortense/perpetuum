import { curry } from './curry'

export const map = curry((fn, struct) => struct.__perpetuum__
  ? struct.constructor(...struct.toArray().map(fn))
  : struct.map(fn))
