export const copy = object => object.__perpetuum__
  ? object.copy()
  : JSON.parse(JSON.stringify(object))
