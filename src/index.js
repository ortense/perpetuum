import { and }     from './functions/and'
import { compose } from './functions/compose'
import { copy }    from './functions/copy'
import { curry }   from './functions/curry'
import { map }     from './functions/map'
import { memoize } from './functions/memoize'
import { or }      from './functions/or'
import { pipe }    from './functions/pipe'
import { reduce }  from './functions/reduce'

import { stack }   from './structs/stack'
import { queue }   from './structs/queue'

export {
  and,
  compose,
  copy,
  curry,
  map,
  memoize,
  reduce,
  or,
  pipe,
  queue,
  stack,
}
