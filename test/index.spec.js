import * as lib from '../src/index'

const fNames = [
  'and',
  'compose',
  'copy',
  'curry',
  'map',
  'memoize',
  'reduce',
  'or',
  'pipe',
  'queue',
  'stack',
]

describe('lib', () => {
  it('should export all functions', () => {
    expect(Object.keys(lib)).toEqual(fNames)
    fNames.forEach(fName => expect(typeof lib[fName]).toBe('function'))
  })
})
