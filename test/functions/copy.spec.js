import { copy } from '../../src/functions/copy'
import { queue } from '../../src/structs/queue'
import { stack } from '../../src/structs/stack'

describe('#copy', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof copy).toBe('function')
    })

    it('should return the same type as the input value.', () => {
      expect(typeof copy({})).toBe('object')
      expect(Array.isArray(copy([]))).toBeTruthy()
    })
  })

  context('When it receives an array.', () => {
    it('should returns a deep copy of the array', () => {
      const arr = [1, 2, 3]
      expect(copy(arr)).toEqual(arr)
      expect(copy(arr)).not.toBe(arr)
    })
  })

  context('When it receives an object.', () => {
    it('should returns a deep copy of the object', () => {
      const obj = { foo: 'bar' }
      expect(copy(obj)).toEqual(obj)
      expect(copy(obj)).not.toBe(obj)
    })
  })

  context('When it receives a perpetuum queue.', () => {
    it('should returns a deep copy of the queue', () => {
      const q = queue(1, 2, 3)
      const c = copy(q)
      expect(c.toString()).toEqual(q.toString())
      expect(c.toArray()).toEqual(q.toArray())
      expect(c.toArray()).not.toBe(q.toArray())
    })
  })

  context('When it receives a perpetuum stack.', () => {
    it('should returns a deep copy of the stack', () => {
      const s = stack(1, 2, 3)
      const c = copy(s)
      expect(c.toString()).toEqual(s.toString())
      expect(c.toArray()).toEqual(s.toArray())
      expect(c.toArray()).not.toBe(s.toArray())
    })
  })
})
