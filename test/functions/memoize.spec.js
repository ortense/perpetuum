import { memoize } from '../../src/functions/memoize'

const double = jest.fn().mockImplementation(x => x * 2)

describe('#memoize', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof memoize).toBe('function')
    })

    it('should return a function.', () => {
      expect(typeof memoize(x => x)).toBe('function')
    })
  })

  context('When it receives a function.', () => {
    it('should returns a memoizes functions.', () => {
      const memo = memoize(double)
      expect(memo(2)).toBe(4)
      expect(memo(2)).toBe(4)
      expect(double.mock.calls.length).toBe(1)
      expect(memo(3)).toBe(6)
      expect(memo(3)).toBe(6)
      expect(memo(3)).toBe(6)
      expect(double.mock.calls.length).toBe(2)
    })
  })
})

