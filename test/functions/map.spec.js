import { map } from '../../src/functions/map'
import { queue } from '../../src/structs/queue'
import { stack } from '../../src/structs/stack'

const increment = n => n + 1

describe('#map', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof map).toBe('function')
    })

    it('should return the same type as the input value.', () => {
      expect(Array.isArray(map(increment, []))).toBeTruthy()
      expect(map(increment, queue(1, 2, 3)).is(queue)).toBeTruthy()
      expect(map(increment, stack(1, 2, 3)).is(stack)).toBeTruthy()
    })
  })

  context('When it receives only one function per parameter.', () => {
    it('should be a curred function', () => {
      expect(typeof map(increment)).toBe('function')
    })
  })

  context('When it receives an array', () => {
    it('should return a new mapped array', () => {
      const array = [1, 2, 3]
      expect(Array.isArray(map(increment, array))).toBeTruthy()
      expect(map(increment, array)).toEqual([2, 3, 4])
      expect(map(increment)(array)).toEqual([2, 3, 4])
    })

    it('don\'t should modify the original array', () => {
      const array = [1, 2, 3]
      const mapped = map(increment, array)
      expect(array).not.toBe(mapped)
      expect(array).toEqual([1, 2, 3])
      expect(mapped).toEqual([2, 3, 4])
    })
  })

  context('When it receives a queue', () => {
    it('should return a new mapped queue', () => {
      const q = queue(1, 2, 3)
      expect(map(increment, q).is(queue)).toBeTruthy()
      expect(map(increment, q).toArray()).toEqual([2, 3, 4])
      expect(map(increment)(q).toArray()).toEqual([2, 3, 4])
    })
  })

  context('When it receives a stack', () => {
    it('should return a new mapped stack', () => {
      const s = stack(1, 2, 3)
      expect(map(increment, s).is(stack)).toBeTruthy()
      expect(map(increment, s).toArray()).toEqual([2, 3, 4])
      expect(map(increment)(s).toArray()).toEqual([2, 3, 4])
    })
  })
})
