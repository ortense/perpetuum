import { compose } from '../../src/functions/compose'

const a = x => `a(${x})`
const b = x => `b(${x})`
const c = x => `c(${x})`

describe('#compose', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof compose).toBe('function')
    })

    it('should return a function.', () => {
      expect(typeof compose(a, b, c)).toBe('function')
    })
  })

  context('when call composed function', () => {
    it('should call all functions in the inverse order of the arguments.', () => {
      expect(compose(a)('arg')).toBe('a(arg)')
      expect(compose(a, a)('arg')).toBe('a(a(arg))')
      expect(compose(a, b)('arg')).toBe('a(b(arg))')
      expect(compose(a, b, c)('arg')).toBe('a(b(c(arg)))')
    })
  })

  context('when an argument is not a function', () => {
    it('should throw "argument is not a function".', () => {
      expect(compose(a, null, b)).toThrow(TypeError, /.*not a function$/)
    })
  })
})
