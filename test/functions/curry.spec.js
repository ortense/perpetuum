import { curry } from '../../src/functions/curry'

const add = (x, y, z) => x + y + z

describe('#curry', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof curry).toBe('function')
    })

    it('should return a function.', () => {
      expect(typeof curry(x => x)).toBe('function')
    })
  })

  context('When it receives a function.', () => {
    it('should returns a curred function.', () => {
      expect(curry(add)(1, 2, 3)).toBe(6)
      expect(curry(add)(1)(2)(3)).toBe(6)
      expect(curry(add)(1, 2)(3)).toBe(6)
      expect(curry(add)(1)(2, 3)).toBe(6)
    })
  })

  context('When it receives more arguments than expected by original function.', () => {
    it('should call original function', () => {
      expect(curry(add)(1, 2, 3, 4)).toBe(6)
      expect(curry(add)(1, 2)(3, 4)).toBe(6)
      expect(curry(add)(1)(2, 3, 4)).toBe(6)
    })
  })
})
