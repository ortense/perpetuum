import { pipe } from '../../src/functions/pipe'

const a = x => `a(${x})`
const b = x => `b(${x})`
const c = x => `c(${x})`

describe('#pipe', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof pipe).toBe('function')
    })

    it('should return a function.', () => {
      expect(typeof pipe(a, b, c)).toBe('function')
    })
  })

  context('when call piped function', () => {
    it('should call all functions in the order of the arguments.', () => {
      expect(pipe(a)('arg')).toBe('a(arg)')
      expect(pipe(a, a)('arg')).toBe('a(a(arg))')
      expect(pipe(a, b)('arg')).toBe('b(a(arg))')
      expect(pipe(a, b, c)('arg')).toBe('c(b(a(arg)))')
    })
  })

  context('when an argument is not a function', () => {
    it('should throw "argument is not a function".', () => {
      expect(pipe(a, null, b)).toThrow(TypeError, /.*not a function$/)
    })
  })
})
