import { or } from '../../src/functions/or'

const truthy = jest.fn().mockReturnValue(true)
const falsy = jest.fn().mockReturnValue(false)

describe('#or', () => {
  afterEach(() => {
    truthy.mockClear()
    falsy.mockClear()
  })

  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof or).toBe('function')
    })

    it('should return a function', () => {
      expect(typeof or()).toBe('function')
    })
  })

  context('When returned function is called', () => {
    it('should execute all the comparison functions until return true.', () => {
      const f = or(falsy, falsy, truthy, truthy, falsy)
      const result = f()
      expect(result).toBeTruthy()
      expect(falsy).toHaveBeenCalledTimes(2)
      expect(truthy).toHaveBeenCalledTimes(1)
    })

    it('should call comparison functions with received args', () => {
      const f = or(falsy, truthy)
      expect(f('arg1', 'arg2')).toBeTruthy()
      expect(falsy.mock.calls).toEqual([['arg1','arg2']])
      expect(truthy.mock.calls).toEqual([['arg1','arg2']])
    })
  })

  context('basic logic test', () => {
    it('(true || true) === true', () => {
      expect(or(() => true, () => true)()).toBeTruthy()
    })

    it('(false || true) === true', () => {
      expect(or(() => false, () => true)()).toBeTruthy()
    })

    it('(false || false) === false', () => {
      expect(or(() => false, () => false)()).toBeFalsy()
    })

    it('(true || false) === false', () => {
      expect(or(() => true, () => false)()).toBeTruthy()
    })
  })
})
