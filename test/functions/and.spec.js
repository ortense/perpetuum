import { and } from '../../src/functions/and'

const truthy = jest.fn().mockReturnValue(true)
const falsy = jest.fn().mockReturnValue(false)

describe('#and', () => {
  afterEach(() => {
    truthy.mockClear()
    falsy.mockClear()
  })

  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof and).toBe('function')
    })

    it('should return a function.', () => {
      expect(typeof and()).toBe('function')
    })
  })

  context('When returned function is called', () => {
    it('should execute all the comparison functions as while they return true.', () => {
      const f = and(truthy, truthy, falsy, falsy)
      expect(f()).toBeFalsy()
      expect(truthy).toHaveBeenCalledTimes(2)
      expect(falsy).toHaveBeenCalledTimes(1)
    })

    it('should call comparison functions with received args', () => {
      const f = and(truthy, truthy)
      const result = f('arg1', 'arg2')
      const [firstCallArgs, secondCallArgs] = truthy.mock.calls
      expect(result).toBeTruthy()
      expect(truthy).toHaveBeenCalledTimes(2)
      expect(firstCallArgs).toEqual(['arg1', 'arg2'])
      expect(secondCallArgs).toEqual(['arg1', 'arg2'])
    })
  })

  context('basic logic test', () => {
    it('(true && true) === true', () => {
      expect(and(() => true, () => true)()).toBeTruthy()
    })

    it('(false && true) === false', () => {
      expect(and(() => false, () => true)()).toBeFalsy()
    })

    it('(false && false) === false', () => {
      expect(and(() => false, () => false)()).toBeFalsy()
    })

    it('(true && false) === false', () => {
      expect(and(() => true, () => false)()).toBeFalsy()
    })
  })
})
