import { reduce } from '../../src/functions/reduce'
import { queue } from '../../src/structs/queue'
import { stack } from '../../src/structs/stack'

const reducer = (x, y) => x + y

describe('#reduce', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof reduce).toBe('function')
    })
  })

  context('When it receives part of the arguments.', () => {
    it('should be a curred function', () => {
      const initial = 0
      const array = [1, 2, 3]
      expect(typeof reduce(reducer)).toBe('function')
      expect(typeof reduce(reducer)(initial)).toBe('function')
      expect(typeof reduce(reducer)(initial)(array)).toBe('number')
      expect(typeof reduce(reducer, initial, array)).toBe('number')
    })
  })

  context('When it receives an array', () => {
    it('should receduce the array to a value', () => {
      const initial = 1
      const array = [1, 2, 3]
      expect(reduce(reducer, initial, array)).toBe(7)
    })
  })

  context('When initial value is null', () => {
    it('should receduce the array to a value ignoring the initial.', () => {
      const array = [1, 2, 3]
      expect(reduce(reducer, null, array)).toBe(6)
    })
  })

  context('When it receives a queue', () => {
    it('should receduce the queue to a value', () => {
      const initial = 0
      const q = queue(1, 2, 3)
      expect(reduce(reducer, initial, q)).toBe(6)
    })
  })

  context('When it receives a stack', () => {
    it('should receduce the stack to a value', () => {
      const initial = 0
      const s = stack(1, 2, 3)
      expect(reduce(reducer, initial, s)).toBe(6)
    })
  })
})
