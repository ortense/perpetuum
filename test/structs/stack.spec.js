import { stack } from '../../src/structs/stack'

describe('#stack', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof stack).toBe('function')
    })

    it('should return an object.', () => {
      expect(typeof stack()).toBe('object')
    })
  })

  context('When create a stack', () => {
    it('Should return a stack object', () => {
      const s = stack(1, 2, 3)
      expect(typeof s.add).toBe('function')
      expect(typeof s.remove).toBe('function')
      expect(typeof s.toString).toBe('function')
      expect(typeof s.copy).toBe('function')
      expect(typeof s.is).toBe('function')
      expect(typeof s.toArray).toBe('function')
      expect(s.length).toBe(3)
      expect(s.constructor).toBe(stack)
      expect(s.__perpetuum__).toBeTruthy()
    })
  })

  context('When parse stack to string', () => {
    it('Should format the stack', () => {
      const actual = stack(1, 2, 3)
      const expected = 'stack [1 2 3]'
      expect(actual.toString()).toEqual(expected)
      expect(`${actual}`).toEqual(expected)
      expect(actual + '').toEqual(expected)
    })
  })

  context('When add a value on the stack', () => {
    it('Should return a new stack with added value', () => {
      const initialStack = stack(1, 2)
      const secondStack = initialStack.add(3)
      expect(initialStack.length).toEqual(2)
      expect(initialStack.toArray()).toEqual([1, 2])
      expect(initialStack.toString()).toEqual('stack [1 2]')
      expect(secondStack.length).toEqual(3)
      expect(secondStack.toArray()).toEqual([1, 2, 3])
      expect(secondStack.toString()).toEqual('stack [1 2 3]')
    })
  })

  context('When remove a value from stack', () => {
    it('Should return a new stack without last value', () => {
      const initialStack = stack(1, 2)
      const secondStack = initialStack.remove()
      expect(initialStack.length).toEqual(2)
      expect(initialStack.toArray()).toEqual([1, 2])
      expect(initialStack.toString()).toEqual('stack [1 2]')
      expect(secondStack.length).toEqual(1)
      expect(secondStack.toArray()).toEqual([1])
      expect(secondStack.toString()).toEqual('stack [1]')
    })
  })

  context('When copy a stack', () => {
    it('Should return a deep copy from copied stack', () => {
      const initial = stack(1, 2)
      const copied = initial.copy()
      expect(initial.length).toEqual(copied.length)
      expect(initial.toArray()).toEqual(copied.toArray())
      expect(initial.toArray()).not.toBe(copied.toArray())
      expect(initial.toString()).toEqual(copied.toString())
      expect(initial).not.toBe(copied)
    })
  })

  context('When checking if a stack is a stack.', () => {
    it('Should be true', () => {
      const s = stack(1, 2)
      expect(s.is(stack)).toBeTruthy()
    })
  })
})
