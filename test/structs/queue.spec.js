import { queue } from '../../src/structs/queue'

describe('#queue', () => {
  context('smoke testing.', () => {
    it('should be a function.', () => {
      expect(typeof queue).toBe('function')
    })

    it('should return an object.', () => {
      expect(typeof queue()).toBe('object')
    })
  })

  context('When create a queue', () => {
    it('Should return a queue object', () => {
      const q = queue(1, 2, 3)
      expect(typeof q.add).toBe('function')
      expect(typeof q.remove).toBe('function')
      expect(typeof q.toString).toBe('function')
      expect(typeof q.copy).toBe('function')
      expect(typeof q.is).toBe('function')
      expect(typeof q.toArray).toBe('function')
      expect(q.length).toBe(3)
      expect(q.constructor).toBe(queue)
      expect(q.__perpetuum__).toBeTruthy()
    })
  })

  context('When parse queue to string', () => {
    it('Should format the queue', () => {
      const actual = queue(1, 2, 3)
      const expected = 'queue [1 2 3]'
      expect(actual.toString()).toBe(expected)
      expect(`${actual}`).toBe(expected)
      expect(actual + '').toBe(expected)
    })
  })

  context('When add a value on the queue', () => {
    it('Should return a new queue with added value', () => {
      const initialQueue = queue(1, 2)
      const secondQueue = initialQueue.add(3)
      expect(initialQueue.length).toBe(2)
      expect(initialQueue.toArray()).toEqual([1, 2])
      expect(initialQueue.toString()).toEqual('queue [1 2]')
      expect(secondQueue.length).toBe(3)
      expect(secondQueue.toArray()).toEqual([1, 2, 3])
      expect(secondQueue.toString()).toBe('queue [1 2 3]')
    })
  })

  context('When remove a value from queue', () => {
    it('Should return a new queue without first value', () => {
      const initialQueue = queue(1, 2)
      const secondQueue = initialQueue.remove()
      expect(initialQueue.length).toBe(2)
      expect(initialQueue.toArray()).toEqual([1, 2])
      expect(initialQueue.toString()).toEqual('queue [1 2]')
      expect(secondQueue.length).toBe(1)
      expect(secondQueue.toArray()).toEqual([2])
      expect(secondQueue.toString()).toBe('queue [2]')
    })
  })

  context('When copy a queue', () => {
    it('Should return a deep copy from copied queue', () => {
      const initial = queue(1, 2)
      const copied = initial.copy()
      expect(initial.length).toBe(copied.length)
      expect(initial.toArray()).toEqual(copied.toArray())
      expect(initial.toArray()).not.toBe(copied.toArray())
      expect(initial.toString()).toBe(copied.toString())
      expect(initial).not.toBe(copied)
    })
  })

  context('When checking if a queue is a queue.', () => {
    it('Should be true', () => {
      const q = queue(1, 2)
      expect(q.is(queue)).toBeTruthy()
    })
  })
})
