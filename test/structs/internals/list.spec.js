import { append, base, is } from '../../../src/structs/internals/list'

describe('#list structs internals', () => {
  describe('base', () => {
    context('smoke testing.', () => {
      it('should be a function.', () => {
        expect(typeof base).toBe('function')
      })

      it('should return an object.', () => {
        expect(typeof base()).toBe('object')
      })
    })

    context('When create a base list struct', () => {
      it('Should return an array wrapper with basic properties', () => {
        const list = base(1, 2, 3)
        expect(typeof list.get).toBe('function')
        expect(typeof list.toArray).toBe('function')
        expect(list.length).toBe(3)
        expect(list.__perpetuum__).toBeTruthy()
      })
    })

    describe('base.get', () => {
      context('When called with a number', () => {
        it('should return an element in the received argument index.', () => {
          const list = base(1, 2)
          expect(list.get(0)).toBe(1)
          expect(list.get(1)).toBe(2)
          expect(list.get(2)).toBeUndefined()
        })
      })
    })

    describe('base.toArray', () => {
      context('When called', () => {
        it('should return an array with initial arguments', () => {
          const args = [2, 1, 0]
          const list = base(...args)
          expect(list.toArray()).toEqual(args)
          expect(base().toArray()).toEqual([])
        })
      })
    })

    describe('base.length', () => {
      context('When it is accessed', () => {
        it('should be equal the number of arguments.', () => {
          const args = [2, 1, 0]
          const list = base(...args)
          expect(list.length).toEqual(args.length)
          expect(list.toArray().length).toEqual(args.length)
        })
      })
    })
  })

  describe('append', () => {
    const wrapper = (...args) => args
    const value = 1
    const array = []
    context('smoke testing.', () => {
      it('should be a function.', () => {
        expect(typeof append).toBe('function')
      })

      it('should return wrapped value', () => {
        expect(Array.isArray(append(wrapper, value, array))).toBeTruthy()
      })
    })

    context('When is correct colled', () => {
      it('should return a new list with the value added to it', () => {
        expect(append(wrapper, value, [0, 2])).toEqual([0, 2, 1])
      })
    })
  })

  describe('is', () => {
    context('smoke testing.', () => {
      it('should be a function.', () => {
        expect(typeof is).toBe('function')
      })

      it('should return a function', () => {
        expect(typeof is()).toBe('function')
      })
    })

    context('When call return function', () => {
      it('should compare the inicial argument with the new argument', () => {
        const value = {}
        expect(is(value)(value)).toBeTruthy()
        expect(is(value)({})).toBeFalsy()
      })
    })
  })
})
